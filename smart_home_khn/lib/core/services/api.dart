import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:smart_home_khn/core/models/user.dart';
import 'package:smart_home_khn/core/models/device.dart';
import 'package:smart_home_khn/core/viewmodels/widgets/device_model.dart';

/// The service responsible for networking requests
class Api {
  static const endpoint = 'http://my-json-server.typicode.com/leman7193/fake_json';
  
  var client = new http.Client();

  Future<User> getUserProfile(int userId) async {
    // Get user profile for id
    var response = await client.get('$endpoint/users/$userId');

    // Convert and return
    return User.fromJson(json.decode(response.body));
  }

   Future<List<Device>> getDevicesForUser(int userId) async {
    var devices = List<Device>();
    print('URI: $endpoint/devices?userId=$userId');
    // Get user posts for id
    var response = await client.get('$endpoint/devices?userId=$userId');

    // parse into List
    var parsed = json.decode(response.body) as List<dynamic>;

    // loop and convert each item to Post
    for (var device in parsed) {
      devices.add(Device.fromJson(device));
    }

    return devices;
  }
}
