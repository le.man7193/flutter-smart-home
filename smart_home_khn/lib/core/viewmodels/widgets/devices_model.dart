import 'package:meta/meta.dart';
import 'package:smart_home_khn/core/models/device.dart';
import 'package:smart_home_khn/core/viewmodels/widgets/device_model.dart';
import 'package:smart_home_khn/core/services/api.dart';

import '../base_model.dart';

class DevicesModel extends BaseModel {
  Api _api;

  DevicesModel({
    @required Api api,
  }) : _api = api;

  List<Device> devices;

  Future getDevices(int userId) async {
    setBusy(true);
    devices = await _api.getDevicesForUser(userId);
    setBusy(false);
  }
}
