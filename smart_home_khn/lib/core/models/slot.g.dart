// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'slot.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Slot _$SlotFromJson(Map<String, dynamic> json) {
  return Slot(
    json['active'] as bool,
  )
    ..index = json['index'] as int
    ..name = json['name'] as String;
}

Map<String, dynamic> _$SlotToJson(Slot instance) => <String, dynamic>{
      'index': instance.index,
      'name': instance.name,
      'active': instance.active,
    };
