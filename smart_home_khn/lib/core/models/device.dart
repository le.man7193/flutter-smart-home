import 'slot.dart';
import 'package:json_annotation/json_annotation.dart';
part 'device.g.dart';


@JsonSerializable(nullable: false)
class Device {
  String deviceId;
  int userId;
  String status;
  List<Slot> slots;

  Device({this.deviceId, this.userId, this.status, this.slots});

  factory Device.fromJson(Map<String, dynamic> json) => _$DeviceFromJson(json);

  Map<String, dynamic> toJson() => _$DeviceToJson(this);
}