// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'device.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Device _$DeviceFromJson(Map<String, dynamic> json) {
  return Device(
    deviceId: json['deviceId'] as String,
    userId: json['userId'] as int,
    status: json['status'] as String,
    slots: (json['slots'] as List)
        .map((e) => Slot.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$DeviceToJson(Device instance) => <String, dynamic>{
      'deviceId': instance.deviceId,
      'userId': instance.userId,
      'status': instance.status,
      'slots': instance.slots,
    };
