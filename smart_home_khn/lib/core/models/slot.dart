import 'package:json_annotation/json_annotation.dart';
part 'slot.g.dart';

@JsonSerializable(nullable: false)
class Slot {
  int index;
  String name;
  bool active;

  Slot( this.active);

  factory Slot.fromJson(Map<String, dynamic> json) => _$SlotFromJson(json);
  Map<String, dynamic> toJson() => _$SlotToJson(this);
}