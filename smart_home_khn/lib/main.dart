import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:smart_home_khn/provider_setup.dart';
import 'package:smart_home_khn/ui/router.dart';

import 'core/constants/app_constants.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: providers,
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
        primaryColor: Colors.green,
        // fontFamily: UIData.quickFont,
        primarySwatch: Colors.amber),
        initialRoute: RoutePaths.Login,
        onGenerateRoute: Router.generateRoute,
      ),
    );
  }
}
