import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:smart_home_khn/core/models/device.dart';
import 'package:smart_home_khn/core/models/slot.dart';
import 'package:smart_home_khn/core/models/user.dart';
import 'package:smart_home_khn/core/viewmodels/widgets/device_model.dart';
import 'package:smart_home_khn/ui/share/ui_helpers.dart';
import 'package:smart_home_khn/core/services/mqtt_stream.dart';
import 'package:smart_home_khn/ui/views/base_widget.dart';

class DeviceView extends StatefulWidget {
  final Device device;
  DeviceView({Key key, this.device}) : super(key: key);

  @override
  _DeviceViewState createState() => _DeviceViewState(device);
}

class _DeviceViewState extends State<DeviceView> {
  Device device;
  _DeviceViewState(this.device);
  AppMqttTransactions myMqtt;

  void onClickSwitch(Slot slot) {
    print('${device.deviceId}/Control/Light0${slot.index}');
    myMqtt.publish('${device.deviceId}/Control/Light0${slot.index}',
        slot.active ? 'off' : 'on');
    
    setState(() {
      slot.active = !slot.active;
    });
    //device.updateDeviceModel(slot);
  }

  @override
  void initState() {
    super.initState();
    myMqtt = AppMqttTransactions();
  }

  @override void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.pop(context, false),
          ),
          title: Text('Smart KHN'),
        ),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              UIHelper.verticalSpaceLarge,
              Text(
                widget.device.deviceId,
                style: TextStyle(
                  color: widget.device.status.compareTo('activated') == 0
                      ? Colors.green
                      : Colors.red,
                  fontSize: 35,
                  fontWeight: FontWeight.w900,
                ),
              ),
              Text(
                'by ${Provider.of<User>(context).name}',
                style: TextStyle(fontSize: 12.0),
              ),
              GridView.count(
                crossAxisCount:
                    MediaQuery.of(context).orientation == Orientation.portrait
                        ? 2
                        : 4,
                shrinkWrap: true,
                children: widget.device.slots
                    .map(
                      (slot) => Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: InkWell(
                          splashColor: Colors.yellow,
                          onTap: () => onClickSwitch(slot),
                          child: Material(
                            clipBehavior: Clip.antiAlias,
                            elevation: 2.0,
                            child: Stack(
                              fit: StackFit.expand,
                              children: <Widget>[
                                Text('${slot.active}'),
                                Text(
                                  '${slot.index}',
                                  style: TextStyle(
                                      fontSize: 20.0, color: Colors.red),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ).toList(),
              ),
            ],
          ),
        ),
      );
  }
}
