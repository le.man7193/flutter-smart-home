import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:smart_home_khn/core/models/user.dart';
import 'package:smart_home_khn/ui/share/text_styles.dart';
import 'package:smart_home_khn/ui/share/ui_helpers.dart';
import 'package:smart_home_khn/ui/widgets/smart_device.dart';

class HomeView extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    User user = Provider.of<User>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Smart Home KHN'),
      ),
      drawer: Drawer(
        child: Column(
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountName: new Text(
                user.name,
                style:
                    new TextStyle(fontSize: 18.0, fontWeight: FontWeight.w500),
              ),
              accountEmail: new Text(
                "${user.username}@gmail.com",
                style:
                    new TextStyle(fontSize: 18.0, fontWeight: FontWeight.w500),
              ),
            ),
            // new Column(children: null),
          ],
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          UIHelper.verticalSpaceLarge,
          Padding(
            padding: const EdgeInsets.only(left: 20.0),
            child: Text(
              'Welcome ${user.name}',
              style: headerStyle,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20.0),
            child:
                Text('Here are all your smart devices', style: subHeaderStyle),
          ),
          UIHelper.verticalSpaceSmall,
          Expanded(
            child: Devices(),
          )
        ],
      ),
    );
  }
}
