import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:smart_home_khn/core/constants/app_constants.dart';
import 'package:smart_home_khn/core/models/device.dart';
import 'package:smart_home_khn/core/viewmodels/widgets/device_model.dart';
import 'package:smart_home_khn/ui/views/home_view.dart';
import 'package:smart_home_khn/ui/views/login_view.dart';
import 'package:smart_home_khn/ui/views/device_view.dart';

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case RoutePaths.Home:
        return MaterialPageRoute(builder: (_) => HomeView());
      case RoutePaths.Login:
        return MaterialPageRoute(builder: (_) => LoginView());
      case RoutePaths.Device:
        var device = settings.arguments as Device;
        return MaterialPageRoute(builder: (_) => DeviceView(device: device));
      default:
        return MaterialPageRoute(
          builder: (_) => Scaffold(
            body: Center(
              child: Text('No route defined for ${settings.name}'),
            ),
          ),
        );
    }
  }
}
