import 'package:flutter/material.dart';
import 'package:smart_home_khn/core/models/device.dart';
import 'package:smart_home_khn/core/viewmodels/widgets/device_model.dart';

class DeviceListItem extends StatelessWidget {
  final Device device;
  final Function onTap;
  const DeviceListItem({this.device, this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 20.0, vertical: 15.0),
        padding: EdgeInsets.all(10.0),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(5.0),
            boxShadow: [
              BoxShadow(
                  blurRadius: 3.0,
                  offset: Offset(0.0, 2.0),
                  color: Color.fromARGB(80, 0, 0, 0))
            ]),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              '${device.deviceId}',
              style: TextStyle(
                fontWeight: FontWeight.w900,
                fontSize: 16.0,
                color: device.status.compareTo('activated') == 0 ? Colors.green : Colors.red, 
              ),
            ),
            Text('${device.status}',
                maxLines: 2, overflow: TextOverflow.ellipsis)
          ],
        ),
      ),
    );
  }
}
