import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:smart_home_khn/core/constants/app_constants.dart';
import 'package:smart_home_khn/core/models/user.dart';
import 'package:smart_home_khn/core/viewmodels/widgets/devices_model.dart';
import 'package:smart_home_khn/ui/views/base_widget.dart';
import 'package:smart_home_khn/ui/widgets/smart_device_list_item.dart';

class Devices extends StatelessWidget {
  const Devices({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ScrollController _scrollController = ScrollController();
    return BaseWidget<DevicesModel>(
      model: DevicesModel(api: Provider.of(context)),
      onModelReady: (model) => model.getDevices(Provider.of<User>(context).id),
      builder: (context, model, child) => model.busy
          ? Center(
              child: CircularProgressIndicator(),
            )
          : ListView.builder(
              physics: const AlwaysScrollableScrollPhysics(),
              controller: _scrollController,
              itemCount: model.devices.length,
              itemBuilder: (context, index) => DeviceListItem(
                device: model.devices[index],
                onTap: () {
                  Navigator.pushNamed(
                    context,
                    RoutePaths.Device,
                    arguments: model.devices[index],
                  );
                },
              ),
            ),
    );
  }
}
