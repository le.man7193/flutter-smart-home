import 'package:get_it/get_it.dart';
import './viewmodel/login_model.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  //singleton for services and API
  // locator.registerLazySingleton(() => AuthenticationService());
  // locator.registerLazySingleton(() => Api());
  locator.registerLazySingleton(() => LoginModel());

  
  //Factory for models
  // locator.registerFactory(() => LoginModel());
  // locator.registerFactory(() => HomeModel());
  // locator.registerFactory(() => CommentsModel());
}