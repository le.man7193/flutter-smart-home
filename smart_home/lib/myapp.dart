import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:smart_home/src/utils/translations.dart';
import './src/utils/uidata.dart';
import 'src/ui/page/login/login_page.dart';

class MyApp extends StatelessWidget {
  final materialApp = MaterialApp(
    title: UIData.appName,
    theme: ThemeData(
        primaryColor: Colors.black,
        fontFamily: UIData.quickFont,
        primarySwatch: Colors.amber),
    debugShowCheckedModeBanner: false,
    showPerformanceOverlay: false,
    home: null,
    localizationsDelegates: [
      // ... app-specific localization delegate[s] here
      const TranslationsDelegate(),
      GlobalMaterialLocalizations.delegate,
      GlobalWidgetsLocalizations.delegate,
    ],
    supportedLocales: [
        const Locale("en", "US"),
        const Locale("vi", "VN"),
      ],
      initialRoute: UIData.login,
      routes: <String , WidgetBuilder>{
        UIData.login : (BuildContext context) => LoginPage(),
      },

  );

  @override
  Widget build(BuildContext context) {
    return materialApp;
  }
}
